<?php
    echo "Ejercicio: Validar si un email es correcto <br/>";
    echo "Correo a validar si es correcto o no: juan1234@hotmail.com <br/>";
    $correo1="juan1234@hotmail.com";
    $resultado4=preg_match('/^(([a-zA-Z0-9-_\.]{1,})+@([a-z]{4,}\.[a-z]{2,6})(\.[a-z]{2})*)$/',$correo1);
    if($resultado4==1){
        echo "El correo es valido<br/>";
    }
    else{
        echo "El correo no es valido <br/>";
    }
    echo "Correo a validar si es correcto o no: juan1234@12123322 <br/>";
    $correo2="juan123@12123322 <br/>";
    $resultado4=preg_match('/^(([a-zA-Z0-9-_\.]{1,})+@([a-z]{4,}\.[a-z]{2,6})(\.[a-z]{2})*)$/',$correo2);
    if($resultado4==1){
        echo "El correo es valido<br/>";
    }
    else{
        echo "El correo no es valido <br/>";
    }

    echo "<br/>Ejercicio: Validar si una curp es valida <br/>";
    echo "Curp a validar: ABDF012345EFGHIJ03 <br/>";
    $curp="ABDF012345EFGHIJ03";
    $resultado2=preg_match('/^([A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2})$/',$curp);
    if($resultado2==1){
        echo "La curp es valida<br/>";
    }
    else{
        echo "La curp no es valida <br/>";
    }
    echo "Curp a validar: 1234ABCDEF567890HJ <br/>";
    $curp="1234ABCDEF567890HJ";
    $resultado2=preg_match('/^([A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2})$/',$curp);
    if($resultado2==1){
        echo "La curp es valido<br/>";
    }
    else{
        echo "La curp no es valido <br/>";
    }

    echo "<br/>Ejercicio: verificar si una cadena tiene mas de 50 caracteres y esta formado solo por letras <br/>";
    $cadena="EstacadenaTieneMasDeCincuentaCaracteresabcdefghijklmno";
    echo "Cadena a validar   "; print_r($cadena); echo "<br/>"; 
    $resultado=preg_match('/^([a-zA-z]{50,})/', $cadena);
    if($resultado==1){
        echo "La cadena tiene mas de 50 caracteres y no tiene numeros<br/>";
    }
    else{
        echo "La cadena no tiene 50 caracteres o tiene numeros <br/>";
    }

    $cadena="Hola23453";
    echo "Cadena a validar   "; print_r($cadena); echo "<br/>"; 
    $resultado=preg_match('/^([a-zA-z]{50,})/', $cadena);
    if($resultado==1){
        echo "La cadena tiene mas de 50 caracteres y no tiene numeros<br/>";
    }
    else{
        echo "La cadena no tiene 50 caracteres o tiene numeros <br/>";
    }
    
    echo "<br/>Ejercicio: validar si un numero tiene decimales <br/>";
    $numero="5689.85";
    echo "numero a validar   "; print_r($numero); echo "<br/>"; 
    $resultado3=preg_match('/^(([0-9]{1,})\.([0-9]{1,}))$/',$numero);
    if($resultado3==1){
        echo "El numero tiene decimales <br/>";
    }
    else{
        echo "El numero no tiene decimales <br/>";
    }
    $numero="100";
    echo "numero a validar   "; print_r($numero); echo "<br/>"; 
    $resultado3=preg_match('/^(([0-9]{1,})\.([0-9]{1,}))$/',$numero);
    if($resultado3==1){
        echo "El numero tiene decimales <br/>";
    }
    else{
        echo "El numero no tiene decimales <br/>";
    }
    
?>