<!DOCTYPE html>
<?php
    session_start();
        $var=$_SESSION['num_cta'];
        if($var==null || $var== ''){
            echo "Usted no tiene permiso para ver esta pagina";
            die();
        } else if($_SESSION['num_cta'] != $_SESSION['Usuarios'][0]['num_cta'] || $_SESSION['contrasena'] != $_SESSION['Usuarios'][$_SESSION['indice']]['contrasena']){
            echo "Usted no tiene permiso para ver esta pagina";
            echo "<br/>";
            echo "<a href='info.php'>Regresar a inicio</a>";
            die();
        }
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
    <title>Agregar</title>
</head>
<body>
<header>
    <div class="contenedor-principal">
        <nav class="barra">
            <a href="info.php">Home</a>
            <a href="formulario.php">Registrar Alumno</a>
            <a href="cerrarsesion.php">Cerrar Sesión</a>
        </nav>
        </div>
    </header>

    <main class="contenedor">
        <form action="guardar.php" method="post">
        <fieldset>
            <legend>Información de alumno</legend>
            <label for="num_cta">Numero de cuenta: </label>
            <input type="number" name="num_cta" require><br>
            <label for="nombre">Nombre: </label>
            <input type="text" name="nombre" require><br>
            <label for="primer_apellido">Primer apellido: </label>
            <input type="text" name="primer_apellido" require><br>
            <label for="segundo_apellido">Segundo apellido: </label>
            <input type="text" name="segundo_apellido"><br>
            <label for="contrasena">Contraseña: </label>
            <input type="text" name="contrasena" require><br>
            <label for="genero">Genero: </label>
            <select name="genero"><br>
                <option disabled selected>--Seleccione--</option>
                <option value="hombre">Hombre</option>
                <option value="mujer">Mujer</option>
                <option value="otro">Otro</option>
            </select><br>
            <label for="fecha_nac">Fecha de Nacimiento: </label>
            <input type="date" name="fecha_nac">

        </fieldset>
        <input type="submit" value="Registrar">
        </form>
    </main>
    
</body>
</html>