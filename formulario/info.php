<?php
    session_start();
    $var=$_SESSION['num_cta'];
    if($var==null || $var== ''){
        echo "Usted no tiene permiso para ver esta pagina";
        die();
    } else if($_SESSION['num_cta'] != $_SESSION['Usuarios'][$_SESSION['indice']]['num_cta'] || $_SESSION['contrasena'] != $_SESSION['Usuarios'][$_SESSION['indice']]['contrasena']){
        echo "Usted no tiene permiso para ver esta pagina";
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
    <title>Inicio</title>
</head>
<body>
<header>
    <div class="contenedor-principal">
        <nav class="barra">
            <a href="info.php">Home</a>
            <a href="formulario.php">Registrar Alumno</a>
            <a href="cerrarsesion.php">Cerrar Sesión</a>
        </nav>
        </div>
    </header>
    <main class="contenedor-principal">
        <h1>Usuario Autenticado</h1>
        <h2>Información del usuario</h2>
        <div class="informacion">
            <h3>Nombre: <?php print_r($_SESSION['Usuarios'][$_SESSION['num_cta']]['nombre']); echo " "; print_r($_SESSION['Usuarios'][$_SESSION['num_cta']]['primer_apellido']);?></h3>
            <p>Número de cuenta: <?php print_r($_SESSION['Usuarios'][$_SESSION['num_cta']]['num_cta']);?></p>
            <p>Fecha de nacimiento: <?php print_r($_SESSION['Usuarios'][$_SESSION['num_cta']]['fecha_nac']);?></p>
        </div>
    </main>

    <section class="contenedor-principal">
        <h2>Datos Guardados</h2>
        <table>
            <tr>
                <th class="ftw-700">#</th>
                <th class="ftw-700">Nombre: </th>
                <th class="ftw-700">Fecha de nacimiento</th>
            </tr>
            <?php
                for($i=0;$i<count($_SESSION['Usuarios']);$i++){
                    echo "<tr>";
                    echo "<th>";
                    print_r($_SESSION['Usuarios'][$i]['num_cta']);
                    echo "</th>";
                    echo "<th>";
                    print_r($_SESSION['Usuarios'][$i]['nombre']); echo " ";
                    print_r($_SESSION['Usuarios'][$i]['primer_apellido']);echo " ";
                    print_r($_SESSION['Usuarios'][$i]['segundo_apellido']);
                    echo "</th>";
                    echo "<th>";
                    print_r($_SESSION['Usuarios'][$i]['fecha_nac']);
                    echo "</th>";
                    echo "</th>";
                }
            ?>
        </table>
    </section>
</body>
</html>